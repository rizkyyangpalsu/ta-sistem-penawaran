<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberAtributsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_atribut', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->string('jenis_kelamin')->nullable();
            $table->json('warna_klip')->nullable();
            $table->string('fungsi')->nullable();
            $table->tinyInteger('ukir')->nullable();
            $table->json('body')->nullable();
            $table->tinyInteger('umur')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->json('warna')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_atribut');
    }
}
