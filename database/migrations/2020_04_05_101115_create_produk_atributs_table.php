<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukAtributsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_atribut', function (Blueprint $table) {
            $table->unsignedInteger('produk_id');
            $table->json('jenis_kelamin')->nullable();
            $table->json('warna_klip')->nullable();
            $table->json('fungsi')->nullable();
            $table->tinyInteger('ukir')->nullable();
            $table->json('body')->nullable();
            $table->tinyInteger('min_umur')->nullable();
            $table->tinyInteger('max_umur')->nullable();
            $table->json('pekerjaan')->nullable();
            $table->json('warna')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_atribut');
    }
}
