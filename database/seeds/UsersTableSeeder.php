<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    protected $data = [
        [
            'name' => 'Admin',
            'email' => 'admin@rekomendasi.com',
            'password' => 'password',
            'role' => 'admin'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            \App\User::query()->create($datum);
        }
    }
}
