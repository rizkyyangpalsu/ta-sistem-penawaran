<?php

use Illuminate\Database\Seeder;

class ProdukTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('data/produk.xlsx');

        \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\ProdukImports(), $file);
    }

}
