<?php

use Illuminate\Database\Seeder;

class MerkTableSeeder extends Seeder
{

    protected $data = [
        [
            'nama' => 'Parker',
            'kode' => 'Par'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            \App\Merk::query()->create($datum);
            $this->command->info("Merk {$datum['nama']} sudah dibuat!");
        }
    }
}
