<?php

use Illuminate\Database\Seeder;

class PenjualanTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('data/penjualan.xls');

        \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\PenjualanImports(), $file);
    }
}
