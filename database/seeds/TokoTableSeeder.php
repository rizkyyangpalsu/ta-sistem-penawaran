<?php

use Illuminate\Database\Seeder;

class TokoTableSeeder extends Seeder
{

    protected $data = [
        [
            'nama' => 'Gramedia TP',
            'kode' => 'P063',
            'alamat' => 'Jl. Basuki Rahmat No.8-12, Kedungdoro, Kec. Tegalsari, Kota SBY, Jawa Timur 60261'
        ], [
            'nama' => 'Gramedia Ciputra',
            'kode' => 'P013',
            'alamat' => 'Ciputra World, Jl. Mayjen Sungkono No.89, Gunung Sari, Dukuhpakis, Surabaya City, East Java 60224'
        ], [
            'nama' => 'Gramedia Royal',
            'kode' => 'P055',
            'alamat' => 'Royal Plaza Lt. UG No. E6, Jl. Ahmad Yani No.16, Wonokromo, Kec. Wonokromo, Kota SBY, Jawa Timur 60243'
        ], [
            'nama' => 'Gramedia Expo',
            'kode' => 'P059',
            'alamat' => 'Jl. Basuki Rahmat No.93-105, Genteng, Kec. Tegalsari, Kota SBY, Jawa Timur 60271'
        ], [
            'nama' => 'Gunung Agung Galaxy Mall',
            'kode' => 'P070',
            'alamat' => 'Galaxy Mall 2 Lt. 4, Jl. Dr. Ir. H. Soekarno No.35-39, Mulyorejo, Surabaya City, East Java 60115'
        ], [
            'nama' => 'Gramedia Manyar',
            'kode' => 'P037',
            'alamat' => 'Jl. Manyar Kertoarjo No.16, Manyar Sabrangan, Kec. Mulyorejo, Kota SBY, Jawa Timur 60116'
        ], [
            'nama' => 'Gramedia PTC',
            'kode' => 'P098',
            'alamat' => 'Pakuwon Mall, Jl. Mayjen Yono Suwoyo No.2, Babatan, Kec. Wiyung, Kota SBY, Jawa Timur 60227'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            \App\Toko::query()->create($datum);
            $this->command->info("Toko {$datum['nama']} sudah dibuat!");
        }
    }
}
