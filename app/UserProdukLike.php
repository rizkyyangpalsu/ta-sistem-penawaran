<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserProdukLike extends Pivot
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_produk_like';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'produk_id',
    ];

}
