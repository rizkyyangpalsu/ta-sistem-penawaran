<?php

namespace App\Imports\Sheets;

use App\Produk;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PenjualanSheets implements ToCollection
{

    protected $toko;

    public function __construct($toko)
    {
        $this->toko = $toko;
    }

    /**
     * @inheritDoc
     */
    public function collection(Collection $collections)
    {
        $collections->splice(0, 5);

        foreach ($collections as $collection) {
            $produk = Produk::query()->firstOrCreate(['kode' => $collection[1]], [
                "merk_id" => 1,
                "nama" => $collection[2],
                "kode" => $collection[1],
                "harga" => $collection[4],
            ]);

            if (!$produk->toko->where('id', $this->toko)->first()) {
                $produk->toko()->attach($this->toko, [
                    'stok' => random_int(0, 30)
                ]);
            }

            $produk->penjualan()->attach($this->toko, [
                'jumlah' => $collection[3]
            ]);
        }
    }
}
