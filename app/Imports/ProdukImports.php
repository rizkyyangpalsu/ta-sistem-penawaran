<?php

namespace App\Imports;

use App\Merk;
use App\Produk;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProdukImports implements ToCollection
{

    protected $header;

    /**
     * @inheritDoc
     */
    public function collection(Collection $collections)
    {
        $this->header = $collections[1];
        $collections->splice(0, 2);

        foreach ($collections as $key => $collection) {
            $merk = Merk::query()->whereKode($collection[3])->first();

            $produk = new Produk();
            $produk->fill([
                "merk_id" => $merk->id,
                "nama" => $collection[2],
                "kode" => $collection[1],
                "harga" => $collection[12],
            ]);

            if ($produk->save()) {

                for ($i = 1; $i <= 7; $i++) {
                    $produk->toko()->attach($i, [
                        'stok' => $collection[3 + 1]
                    ]);
                }

                $kelamin = $this->attributes($collection, [13, 14]);
                $klip = $this->attributes($collection, range(15, 17));
                $fungsi = $this->attributes($collection, [18, 19]);
                $ukir = ($collection[20] == 'v' && $collection[20] != null) ? 1 : 0;
                $body = $this->attributes($collection, range(22, 24));
                $umur = explode(" ", $collection[25]);
                $min_umur = $umur[0];
                $max_umur = $umur[2];
                $pekerjaan = $this->attributes($collection, range(26, 30));
                $warna = $this->attributes($collection, range(31, 44));

                $produk->atribut()->create([
                    "jenis_kelamin" => $kelamin,
                    "warna_klip" => $klip,
                    "fungsi" => $fungsi,
                    "ukir" => $ukir,
                    "body" => $body,
                    "min_umur" => $min_umur,
                    "max_umur" => $max_umur,
                    "pekerjaan" => $pekerjaan,
                    "warna" => $warna
                ]);
            }
        }
    }

    protected function attributes($row, $columns = [])
    {
        $data = [];

        foreach ($columns as $column) {
            if ($row[$column] == 'v') {
                array_push($data, $this->header[$column]);
            }
        }

        return $data;
    }
}
