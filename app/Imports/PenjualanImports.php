<?php

namespace App\Imports;

use App\Imports\Sheets\PenjualanSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PenjualanImports implements WithMultipleSheets
{

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            new PenjualanSheets(1),
            new PenjualanSheets(4),
            new PenjualanSheets(6),
            new PenjualanSheets(5),
            new PenjualanSheets(3),
            new PenjualanSheets(7),
            new PenjualanSheets(2),
        ];
    }
}
