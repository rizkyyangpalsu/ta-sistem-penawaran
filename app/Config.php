<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'configs';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
    ];

}
