<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{

    protected $table = 'toko';

    protected $fillable = [
        "nama",
        "kode",
        "alamat"
    ];

    public function produk()
    {
        return $this->belongsToMany(Produk::class, 'toko_produk', 'toko_id', 'produk_id', 'id', 'id');
    }

}
