<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukAtribut extends Model
{

    protected $table = 'produk_atribut';

    public $timestamps = false;

    protected $fillable = [
        "produk_id",
        "jenis_kelamin",
        "warna_klip",
        "fungsi",
        "ukir",
        "body",
        "min_umur",
        "max_umur",
        "pekerjaan",
        "warna"
    ];

    protected $casts = [
        "jenis_kelamin" => 'array',
        "warna_klip" => 'array',
        "fungsi" => 'array',
        "ukir" => 'bool',
        "body" => 'array',
        "min_umur" => 'int',
        "max_umur" => 'int',
        "pekerjaan" => 'array',
        "warna" => 'array'
    ];

}
