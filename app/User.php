<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\ProdukOrder as Order;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function member()
    {
        return $this->hasOne(MemberAtribut::class, 'user_id', 'id');
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'user_id', 'id');
    }

    public function like()
    {
        return $this->belongsToMany(Produk::class, 'user_produk_like', 'user_id', 'produk_id', 'id', 'id')
            ->using(UserProdukLike::class);
    }

}
