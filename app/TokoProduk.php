<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TokoProduk extends Pivot
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'toko_produk';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'toko_id',
        'produk_id',
    ];

}
