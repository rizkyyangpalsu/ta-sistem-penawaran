<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukOrder extends Model
{

    protected $table = 'produk_order';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'order'
    ];

    protected $casts = [
        'order' => 'array'
    ];
}
