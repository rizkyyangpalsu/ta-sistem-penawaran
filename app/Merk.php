<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{

    protected $table = 'merk';

    protected $fillable = [
        'nama', 'kode'
    ];

    public function produk()
    {
        return $this->hasMany(Produk::class, 'merk_id', 'id');
    }
}
