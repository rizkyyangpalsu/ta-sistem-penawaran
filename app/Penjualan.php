<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Penjualan extends Pivot
{
    protected $table = 'penjualan';

    public $incrementing = true;

    public $timestamps = true;

    protected $hidden = [
        "produk_id",
        "toko_id",
    ];
}
