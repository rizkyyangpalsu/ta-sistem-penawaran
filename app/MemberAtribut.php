<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberAtribut extends Model
{

    protected $table = 'member_atribut';

    public $timestamps = false;

    protected $fillable = [
        "user_id",
        "jenis_kelamin",
        "warna_klip",
        "fungsi",
        "ukir",
        "body",
        "umur",
        "pekerjaan",
        "warna",
    ];

    protected $casts = [
        'warna_klip' => 'array',
        'body' => 'array',
        'warna' => 'array',
    ];

    public function setUmurAttribute($value)
    {
        $this->attributes['umur'] = (int) $value;
    }

}
