<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user.name' => 'required|string',
            'user.email' => 'required|email|unique:users,email',
            'user.password' => 'required|string|confirmed',
            'user.phone' => 'required|string',

            'jenis_kelamin' => [
                'required',
                Rule::in(['pria', 'wanita'])
            ],
            'warna_klip' => 'required|array',
            'fungsi' => [
                'required',
                Rule::in(['menulis', 'tanda tangan'])
            ],
            'ukir' => 'required|boolean',
            'body' => 'required|array',
            'umur' => 'required|integer',
            'pekerjaan' => [
                'required',
                Rule::in(['wiraswasta', 'pengusaha', 'penjabat', 'dosen', 'mahasiswa', 'lainnya'])
            ],
            'warna' => 'required|array',
        ];
    }
}
