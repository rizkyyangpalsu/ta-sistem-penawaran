<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Produk;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function formRegister()
    {
        return view('auth/register');
    }

    public function formLogin()
    {
        return view('auth/login');
    }

    public function login(LoginRequest $loginRequest)
    {
        if (Auth::attempt($loginRequest->validated())) {
            // Authentication passed...
            if (Auth::user()->role == 'admin')
                return redirect()->route('admin');
            else
                return redirect()->route('home');
        }

        return redirect()->back()->withErrors("User tidak ada. Cek kembali email dan password anda");
    }

    public function register(RegisterRequest $registerRequest)
    {
        $userRequest = $registerRequest->validated()['user'];
        $attrRequest = $registerRequest->except(['user', '_token']);

        $tf = [];
        $D = Produk::query()->has('atribut')->count();
        $df = [];
        $hasildf = [];
        $hasildf = [];
        $idf = [];
        $idf1 = [];

        /**
         * Mencari TF
         */
        foreach ($attrRequest as $key => $value) {
            $tf[$key] = [];

            if (is_array($value)) {
                $result = [];

                foreach ($value as $item) {
                    $produkTf = [];
                    foreach (Produk::all() as $produk) {
                        if ($produk->atribut()->exists()) {
                            $hasil = (in_array($item, $produk->atribut->getAttribute($key))) ? 1 : 0;
                            array_push($produkTf, $hasil);
                        }
                    }
                    $result[$item] = $produkTf;
                }

                $tf[$key] = $result;

            } else {
                foreach (Produk::all() as $produk) {
                    if ($produk->atribut()->exists()) {
                        if ($key == 'ukir') {
                            $result = ($produk->atribut->ukir == (int)$value)
                                ? 1
                                : 0;
                        } else if ($key == 'umur') {
                            $result = ((int)$value >= $produk->atribut->min_umur && (int)$value <= $produk->atribut->max_umur)
                                ? 1
                                : 0;
                        } else if ($key == 'pekerjaan' && $value == 'lainnya') {
                            $result = 1;
                        } else if (in_array($value, $produk->atribut->getAttribute($key))) {
                            $result = 1;
                        } else {
                            $result = 0;
                        }

                        array_push($tf[$key], $result);
                    }
                }
            }
        }

        /**
         * Mencari DF, D/DF, IDF, dan IDF+1
         */
        foreach ($tf as $key => $value) {

            if ($key == 'warna_klip' || $key == 'body' || $key == 'warna') {
                $df[$key] = [];
                $hasildf[$key] = [];
                $idf[$key] = [];
                $idf1[$key] = [];
                foreach ($value as $index => $item) {
                    $count = array_count_values($item);

                    if (array_key_exists(1, $count)) {
                        $df[$key][$index] = $count[1];
                        $hasildf[$key][$index] = round($D / $df[$key][$index], 1);
                        $idf[$key][$index] = round(log($hasildf[$key][$index], 10), 3);
                        $idf1[$key][$index] = $idf[$key][$index] + 1;
                    } else {
                        $df[$key][$index] = 0;
                        $hasildf[$key][$index] = 0;
                        $idf[$key][$index] = 0;
                        $idf1[$key][$index] = 1;
                    }
                }
            } else {
                $count = array_count_values($value);

                if (array_key_exists(1, $count)) {
                    $df[$key] = $count[1];
                    $hasildf[$key] = round($D / $df[$key], 1);
                    $idf[$key] = round(log($hasildf[$key], 10), 3);
                    $idf1[$key] = $idf[$key] + 1;
                } else {
                    $df[$key] = 0;
                    $hasildf[$key] = 0;
                    $idf[$key] = 0;
                    $idf1[$key] = 1;
                }
            }
        }

        /**
         * Pencarian W
         */
        $W = [];
        foreach ($tf as $key => $value) {

            if ($key == 'warna_klip' || $key == 'body' || $key == 'warna') {
                $W[$key] = [];
                foreach ($value as $index => $produks) {
                    $W[$key][$index] = [];
                    foreach ($produks as $produk) {
                        array_push($W[$key][$index], $produk * $idf1[$key][$index]);
                    }
                }
            } else {
                $W[$key] = [];
                foreach ($value as $produk) {
                    array_push($W[$key], $produk * $idf1[$key]);
                }
            }

        }

        /**
         * Pembobotan Dokumen
         */
        $dokumen = collect();
        foreach (Produk::has('atribut')->get() as $key => $produk) {
            $bobot = 0;
            foreach (array_keys($W) as $array_key) {
                if ($array_key == 'warna_klip' || $array_key == 'body' || $array_key == 'warna') {
                    foreach ($W[$array_key] as $item) {
                        $bobot += $item[$key];
                    }

                } else {
                    $bobot += $W[$array_key][$key];
                }
            }
            $dokumen->push([
                'nama' => $produk->nama,
                'kode' => $produk->kode,
                'harga' => $produk->harga,
                'bobot' => $bobot
            ]);
        }

        $user = new User();
        $user->fill($userRequest);

        if ($user->save()) {
            $user->order()->create([ 'order' => $dokumen->sortByDesc('bobot') ]);
            $user->member()->create($attrRequest);

            Auth::attempt([
                'email' => $userRequest['email'],
                'password' => $userRequest['password']
            ]);

            return redirect()->route('home');
        }

        return redirect()->back();

    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }

}
