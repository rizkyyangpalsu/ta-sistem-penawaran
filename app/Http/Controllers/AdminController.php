<?php

namespace App\Http\Controllers;

use App\Config;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::where('role', '!=', 'admin')->get();

        return view('admin.dashboard', ['users' => $users]);
    }

    public function config()
    {
        $config = Config::query()->firstWhere('key', 'about');

        return view('admin.config', ['config' => $config]);
    }

    public function storeConfig(Request $request)
    {
        $attributes = Validator::make($request->except('_token'), [
            'config' => 'required'
        ]);

        if ($attributes->fails()) {
            return redirect()->back()->withErrors($attributes->errors());
        }

        $config = Config::query()->firstWhere('key', '=', 'about');
        $config->fill([
            'value' => $request->input('config')
        ]);
        $config->save();

        return redirect()->back();
    }
}
