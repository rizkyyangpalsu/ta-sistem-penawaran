<?php

namespace App\Http\Controllers;

use App\Config;
use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index()
    {
        if (Auth::guest()) {
            $produk = Produk::query()->get();
        } else {
            $produk = collect(auth()->user()->order->order);
        }

        $config = Config::query()->firstWhere('key', 'about');

        return view('home', ['produk' => $produk, 'about' => $config]);
    }

    public function recommendation()
    {
        $produk = collect(auth()->user()->order->order)->sortByDesc('bobot');

        return view('rekomendasi', ['produk' => $produk]);
    }

    public function detail(Produk $produk)
    {
        if (auth()->check()) {
            $produks = collect(auth()->user()->order->order);
            $checkLike = (auth()->user()->like->firstWhere('kode', $produk->kode) !== null) ? true : false;

            view()->share('data', $produks->firstWhere('kode', '=', $produk->kode));
            view()->share('checkLike', $checkLike);

        }

        $data = [
            'produk' => $produk,
            'stok' => $produk->toko->sortBy('id')
        ];

        return view('detail', $data);
    }

    public function like(Produk $produk)
    {
        $user = auth()->user();
        $user->like()->attach($produk);

        return redirect()->route('detail', $produk->kode)->with('success', 'Produk ini telah anda sukai!');
    }

}
