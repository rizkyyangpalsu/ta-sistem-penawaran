<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{

    protected $table = 'produk';

    protected $fillable = [
        "merk_id",
        "nama",
        "kode",
        "harga"
    ];

    protected $appends = [
        'informarsi',
        'informasi_penjualan',
        'total_penjualan'
    ];

    public function merk()
    {
        return $this->belongsTo(Merk::class, 'merk_id', 'id');
    }

    public function toko()
    {
        return $this->belongsToMany(Toko::class, 'toko_produk', 'produk_id', 'toko_id', 'id', 'id')
            ->using(TokoProduk::class)
            ->as('informasi')
            ->withPivot(['stok']);
    }

    public function like()
    {
        return $this->belongsToMany(User::class, 'user_produk_like', 'produk_id', 'user_id', 'id', 'id')
            ->using(UserProdukLike::class);
    }

    public function penjualan()
    {
        return $this->belongsToMany(Toko::class, 'penjualan', 'produk_id', 'toko_id', 'id', 'id')
            ->using(Penjualan::class)
            ->as('informasi_penjualan')
            ->withPivot(['jumlah'])
            ->withTimestamps();
    }

    public function atribut()
    {
        return $this->hasOne(ProdukAtribut::class, 'produk_id', 'id');
    }

    public function getTotalPenjualanAttribute()
    {
        if ($this->penjualan()->exists()) {
            $total = 0;
            foreach ($this->penjualan as $item) {
                $total += $item->informasi_penjualan->jumlah;
            }

            return $total;
        }

        return 0;
    }
}
