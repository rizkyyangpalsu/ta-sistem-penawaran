<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')
    ->name('home');

Route::get('rekomendasi', 'HomeController@recommendation')
    ->name('recommendation')
    ->middleware('auth');
Route::get('detail/{produk}', 'HomeController@detail')
    ->name('detail');
Route::get('like/{produk}', 'HomeController@like')
    ->name('like');

Route::get('register', 'AuthController@formRegister')
    ->name('register');
Route::get('login', 'AuthController@formLogin')
    ->name('login');
Route::post('register', 'AuthController@register')
    ->name('register');
Route::post('login', 'AuthController@login')
    ->name('login');
Route::any('logout', 'AuthController@logout')
    ->name('logout')
    ->middleware('auth');

Route::middleware('auth-admin')->group(function () {
    Route::get('admin', 'AdminController@index')->name('admin');

    Route::get('admin/config', 'AdminController@config')->name('config');
    Route::post('admin/config', 'AdminController@storeConfig')->name('config');
});

Route::bind('produk', function ($value) {
    return \App\Produk::query()->where('kode', '=', $value)->firstOrFail();
});
