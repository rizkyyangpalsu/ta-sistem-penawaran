@extends('skeleton')

@section('content')
    <!-- PURCHASE-ONLINE-AREA START -->
    <div class="purchase-online-area pt-80 product-style-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                </div>
                <div class="col-lg-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="new-arrivals">
                            <div class="row">
                                <!-- Single-product start -->
                                @foreach($produk as $value)
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                        <div class="single-product">
                                            <div class="product-img">
                                                <span class="pro-label sale-label">Best</span>
                                                <a href="{{ route('detail', $value['kode']) }}">
                                                    <img src="{{ asset('images/'.$value['kode'].'.png') }}" alt="" />
                                                </a>
                                            </div>
                                            <div class="product-info clearfix text-center">
                                                <div class="fix">
                                                    <h4 class="post-title-2"><a href="{{ route('detail', $value['kode']) }}">{{ $value['nama'] }}</a></h4>
                                                </div>
                                                <div class="product-action clearfix">
                                                    <h4 class="text-black">Harga: Rp. {{ $value['harga'] }}</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <!-- Single-product end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PURCHASE-ONLINE-AREA END -->
@endsection
