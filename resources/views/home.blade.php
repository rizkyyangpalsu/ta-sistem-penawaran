@extends('skeleton')

@php
    if (auth()->check())
        $best = $produk->sortByDesc('bobot')->take(8);
    else
        $best = $produk->sortBy('total_penjualan', SORT_REGULAR, true)->take(8);

@endphp

@section('content')
    <div class="about-us-area  pt-80 pb-80">
        <div class="container">
            <div class="about-us bg-white">
                <div class="row">
                    <div class="col-12">
                        <div class="bg-dark-white" style="padding: 2em">
                            <h4 class="title-1 title-border text-uppercase mb-30">tentang sistem rekomendasi produk</h4>
                            {!! $about->value !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PURCHASE-ONLINE-AREA START -->
    <div class="purchase-online-area pt-80 product-style-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        @guest
                            <h2 class="title-border">Produk Best Seller Kami</h2>
                        @endguest
                        @auth
                            <h2 class="title-border">Produk Yang Kami Rekomendasikan</h2>
                        @endauth
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                </div>
                <div class="col-lg-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="new-arrivals">
                            <div class="row">
                                <!-- Single-product start -->
                                @foreach($best as $value)
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                        <div class="single-product">
                                            <div class="product-img">
                                                <span class="pro-label sale-label">Best</span>
                                                <a href="{{ route('detail', $value['kode']) }}">
                                                    <img src="{{ asset('images/'.$value['kode'].'.png') }}" alt=""/>
                                                </a>
                                            </div>
                                            <div class="product-info clearfix text-center">
                                                <div class="fix">
                                                    <h4 class="post-title-2"><a
                                                            href="{{ route('detail', $value['kode']) }}">{{ $value['nama'] }}</a>
                                                    </h4>
                                                </div>
                                                <div class="product-action clearfix">
                                                    <h4 class="text-black">Harga: Rp. {{ $value['harga'] }}</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            <!-- Single-product end -->
                            </div>
                            @auth
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <a href="{{ route('recommendation') }}" class="button-one submit-button"
                                           data-text="More">More</a>
                                    </div>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PURCHASE-ONLINE-AREA END -->
@endsection
