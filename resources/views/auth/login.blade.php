@extends('skeleton')

@section('content')
    <div class="container bg-white">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="customer-login text-left">
            <form action="{{ route('login') }}" method="post">
                @csrf
                <h4 class="title-1 title-border text-uppercase mb-30">Login Member</h4>
                <input type="text" name="email" placeholder="Masukkan Email">
                <input type="password"  name="password" placeholder="Masukkan Password">

                <button class="button-one submit-button mt-15" data-text="login" type="submit">login</button>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script type="text/javascript">
        $(document).ready(function () {
            var navListItems = $('div.setup-panel div a'), allWells = $('.setup-content'), allNextBtn = $('.nextBtn');

            allWells.hide();
            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')), $item = $(this);
                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('submit-button').addClass('btn-default');
                    $item.addClass('submit-button');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus()
                }
            });
            allNextBtn.click(function () {
                var curStep = $(this).closest(".setup-content"), curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"), isValid = !0;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = !1;
                        $(curInputs[i]).closest(".form-group").addClass("has-error")
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click')
            });
            $('div.setup-panel div a.submit-button').trigger('click')
        })
    </script>
@endpush
