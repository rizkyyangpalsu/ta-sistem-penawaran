@extends('skeleton')

@push('style')
    <style>
        .stepwizard-step p {
            margin-top: 10px
        }

        .stepwizard-row {
            display: table-row
        }

        .stepwizard {
            display: table;
            width: 100%;
            position: relative
        }

        .stepwizard-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important
        }

        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0
        }

        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px
        }
    </style>
@endpush

@php
    $pekerjaan = ['wiraswasta', 'pengusaha', 'penjabat', 'dosen', 'mahasiswa'];
    $warna = ['hitam', 'ss', 'silver', 'biru', 'gold', 'grey', 'putih', 'coklat', 'pink', 'hijau', 'merah', 'ungu', 'orange', 'kuning'];
@endphp

@section('content')
    <div class="container bg-white">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn submit-button btn-circle">1</a>
                    <p>Step 1</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>Step 2</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p>Step 3</p>
                </div>
            </div>
        </div>
        <form role="form" action="{{ route('register') }}" method="post" style="padding: 2em">
            @csrf
            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> Langkah Pertama: Registrasi</h3>
                        <div class="form-group">
                            <label class="control-label">Masukkan Nama</label>
                            <input maxlength="100" type="text" name="user[name]" required="required" class="form-control"
                                   placeholder="Masukkan Nama"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Masukkan Email</label>
                            <input maxlength="100" type="text" name="user[email]" required="required" class="form-control"
                                   placeholder="Masukkan Email"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Masukkan Password</label>
                            <input maxlength="100" type="password" name="user[password]" required="required"
                                   class="form-control" placeholder="Masukkan Password"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Konfirmasi Password</label>
                            <input maxlength="100" type="password" name="user[password_confirmation]" required="required"
                                   class="form-control" placeholder="Konfirmasi Password"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nomor WA</label>
                            <input type="text" name="user[phone]" required="required"
                                   class="form-control" placeholder="Nomor WA"/>
                        </div>
                        <button class="button-one submit-button nextBtn mt-20 pull-right" data-text="Next" type="button">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> Langkah Kedua : Beritahu Kami Tentang Anda</h3>
                        <div class="form-group">
                            <label class="control-label">Jenis Kelamin</label>
                            <div class="radio">
                                <label><input type="radio" name="jenis_kelamin" value="pria">Pria</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="jenis_kelamin" value="wanita">Wanita</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Umur</label>
                            <input type="number" required="required" class="form-control"
                                   placeholder="Umur" name="umur"/>
                        </div>
                        <div class="form-group">
                            <label for="pekerjaan">Pekerjaan:</label>
                            <select class="form-control" name="pekerjaan" id="pekerjaan">
                                @foreach($pekerjaan as $item)
                                    <option value="{{ $item }}">{{ ucfirst($item) }}</option>
                                @endforeach
                                <option value="lainnya">Lainnya</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Warna Klip : Yang Anda Sukai</label>
                            <div class="checkbox">
                                <label><input type="checkbox" name="warna_klip[]" value="gold">Gold</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="warna_klip[]" value="silver">Silver</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="warna_klip[]" value="limited">Limited</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kegunaan : Yang Anda Butuhkan</label>
                            <div class="radio">
                                <label><input type="radio" name="fungsi" value="menulis">Menulis</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="fungsi" value="tanda tangan" >Tanda tangan</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Body Pena : Yang Anda Sukai</label>
                            <div class="checkbox">
                                <label><input type="checkbox" name="body[]" value="slim">Slim</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="body[]" value="standar">Standar</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="body[]" value="besar">Besar</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="warna">Warna : Yang Anda Sukai</label>
                            @foreach($warna as $item)
                                <div class="checkbox">
                                    <label><input type="checkbox" name="warna[]" value="{{ $item }}">{{ ucfirst($item) }}</label>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label class="control-label">Keperluan Ukir Nama</label>
                            <div class="radio">
                                <label><input type="radio" name="ukir" value="1">Ya</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="ukir" value="0" >Tidak</label>
                            </div>
                        </div>
                        <button class="button-one submit-button nextBtn mt-20 pull-right" data-text="Next" type="button">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> Langkah Ketiga : Pastikan Data Anda Benar</h3>
                        <button class="btn btn-success mt-20 pull-right" data-text="Finish!" type="submit">Finish!</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script type="text/javascript">
        $(document).ready(function () {
            var navListItems = $('div.setup-panel div a'), allWells = $('.setup-content'), allNextBtn = $('.nextBtn');

            allWells.hide();
            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')), $item = $(this);
                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('submit-button').addClass('btn-default');
                    $item.addClass('submit-button');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus()
                }
            });
            allNextBtn.click(function () {
                var curStep = $(this).closest(".setup-content"), curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"), isValid = !0;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = !1;
                        $(curInputs[i]).closest(".form-group").addClass("has-error")
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click')
            });
            $('div.setup-panel div a.submit-button').trigger('click')
        })
    </script>
@endpush
