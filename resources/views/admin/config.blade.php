@extends('skeleton')

@section('content')
    <div class="about-us-area  pt-80 pb-80">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="about-us bg-white">
                <form action="{{ route('config') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <h2>About Form</h2>
                        </div>
                        <div class="col-12">
                            <textarea name="config" rows="5" class="form-control">{!! $config->value !!}</textarea>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 1em;">
                        <div class="col-3">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
