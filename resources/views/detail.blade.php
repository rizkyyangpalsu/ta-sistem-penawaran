@extends('skeleton')

@section('content')
    <!-- PRODUCT-AREA START -->
    <div class="product-area single-pro-area pt-80 pb-80 product-style-2">
        <div class="container">
            <div class="row shop-list single-pro-info no-sidebar">
                <!-- Single-product start -->
                <div class="col-lg-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif
                    <div class="single-product clearfix">
                        <!-- Single-pro-slider Big-photo start -->
                        <div class="single-pro-slider single-big-photo view-lightbox slider-for">
                            <div>
                                <img src="{{ asset('images/'.$produk['kode'].'.png') }}" alt="" />
                                <a class="view-full-screen" href="{{ asset('images/'.$produk['kode'].'.png') }}"  data-lightbox="roadtrip" data-title="{{ $produk['nama'] }}">
                                    <i class="zmdi zmdi-zoom-in"></i>
                                </a>
                            </div>
                        </div>
                        <!-- Single-pro-slider Big-photo end -->
                        <div class="product-info">
                            <div class="fix">
                                <h4 class="post-title floatleft">{{ $produk['nama'] }}</h4>
                            </div>
                            <div class="fix mb-20">
                                <span class="pro-price">Rp. {{ $produk['harga'] }}</span>
                            </div>
                            <div class="product-description">
                                @auth
                                    <table class="table table-bordered text-center">
                                        <thead>
                                        <tr>
                                            <th>Jenis Kelamin</th>
                                            <th>Warna Klip</th>
                                            <th>Fungsi</th>
                                            <th>Ukir</th>
                                            <th>Body</th>
                                            <th>Umur</th>
                                            <th>Pekerjaan</th>
                                            <th>Warna</th>
                                            <th class="text-center">Bobot Produk</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="text-left">
                                            <td>
                                                <ul>
                                                    @foreach($produk->atribut->jenis_kelamin as $item)
                                                        <li>{{ $item }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>
                                                <ul>
                                                    @foreach($produk->atribut->warna_klip as $item)
                                                        <li>{{ $item }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>
                                                <ul>
                                                    @foreach($produk->atribut->fungsi as $item)
                                                        <li>{{ $item }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>{{ ($produk->atribut->ukir) ? "Ya" : "Tidak" }}</td>
                                            <td>
                                                <ul>
                                                    @foreach($produk->atribut->body as $item)
                                                        <li>{{ $item }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>{{ $produk->atribut->min_umur }} - {{ $produk->atribut->max_umur }}</td>
                                            <td>
                                                <ul>
                                                    @foreach($produk->atribut->pekerjaan as $item)
                                                        <li>{{ $item }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>
                                                <ul>
                                                    @foreach($produk->atribut->warna as $item)
                                                        <li>{{ $item }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td class="text-center">{{ $data['bobot'] }}</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="row">
                                        <div class="col-md6">
                                            @if(!$checkLike)
                                                <a href="{{ route('like', $produk['kode']) }}"
                                                   class="button-one submit-button" data-text="Anda menyukai ini?">
                                                    Anda menyukai produk ini?
                                                </a>
                                            @else
                                                <button class="button-one disabled submit-button"
                                                        data-text="Anda telah menyukai produk ini">
                                                    Anda telah menyukai produk ini
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                @endauth
                            </div>

                        </div>
                    </div>
                </div>
                <!-- Single-product end -->
            </div>
            <h1>Stok di beberapa toko:</h1>
            <!-- single-product-tab start -->
            <div class="single-pro-tab">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="single-pro-tab-menu">
                            <!-- Nav tabs -->
                            <ul class="">
                                @foreach($stok as $value)
                                    <li class="@if ($value->id == 1) active @endif">
                                        <a href="#{{ $value->kode }}" data-toggle="tab">{{ $value->nama }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            @foreach($stok as $value)
                                <div class="tab-pane @if ($value->id == 1) active @endif" id="{{ $value->kode }}">
                                    <div class="pro-tab-info pro-description">
                                        <ul>
                                            <li><p>Nama toko &nbsp;&nbsp;&nbsp;: {{ $value->nama }}</p></li>
                                            <li><p>Alamat toko : {{ $value->alamat }}</p></li>
                                            <li><h4>Stok yang tersedia: {{ $value->informasi->stok }}</h4></li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- single-product-tab end -->
        </div>
    </div>
@endsection
