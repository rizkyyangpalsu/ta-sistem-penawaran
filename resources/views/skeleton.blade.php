<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sistem Rekomendasi Produk</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
    <!-- nivo-slider css -->
    <link rel="stylesheet" href="{{ asset('lib/css/nivo-slider.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/css/preview.css') }}">
    <!-- slick css -->
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <!-- lightbox css -->
    <link rel="stylesheet" href="{{ asset('css/lightbox.min.css') }}">
    <!-- material-design-iconic-font css -->
    <link rel="stylesheet" href="{{ asset('css/material-design-iconic-font.css') }}">
    <!-- All common css of theme -->
    <link rel="stylesheet" href="{{ asset('css/default.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <!-- shortcode css -->
    <link rel="stylesheet" href="{{ asset('css/shortcode.css') }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- modernizr css -->
    <script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>
    @stack('style')
</head>
<body>

<!-- WRAPPER START -->
<div class="wrapper bg-dark-white">

    <!-- HEADER-AREA START -->
    <header id="sticky-menu" class="header header-2">
        <div class="header-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4 col-xs-7">
                        <div class="logo text-center">
                            <a href="{{ (auth()->check() && auth()->user()->role == 'admin') ? route('admin') : route('home') }}"><h1 class="title-border text-center">Sistem Rekomendasi Produk</h1></a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-5">
                        <div class="mini-cart text-right">
                            @auth
                                <ul>
                                    @if(auth()->user()->role == 'admin')
                                        <li>
                                            <a href="{{ route('admin') }}" style="padding: 2em" class="cart-icon font-16px">
                                                Dashboard
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('config') }}" class="cart-icon font-16px">
                                                Config
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a class="cart-icon font-16px" style="padding: 2em" href="{{ route('logout') }}">
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            @endauth
                            @guest
                            <ul>
                                <li>
                                    <a class="cart-icon font-16px" style="padding: 2em" href="{{ route('login') }}">
                                        Masuk
                                    </a>
                                </li>
                                <li>
                                    <a class="cart-icon font-16px" href="{{ route('register') }}">
                                        Registrasi
                                    </a>
                                </li>
                            @endguest
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER-AREA END -->
    @yield('content')
    <!-- BRAND-LOGO-AREA START -->
    <div class="brand-logo-area pt-80">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    @guest
                        <a href="{{ route('register') }}">
                            <button class="btn btn-success mt-20" data-text="Coba Fitur Rekomendasi Kami" type="submit">Coba Fitur Rekomendasi Kami</button>
                        </a>
                    @endguest
                </div>
            </div>
        </div>
    </div>
    <!-- BRAND-LOGO-AREA END -->
    <!-- FOOTER START -->
    <footer>
        <!-- Copyright-area start -->
        <div class="copyright-area copyright-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="copyright">
                            <p class="mb-0">&copy; <a href="#" target="_blank">Sistem Rekomendasi Produk </a> 2020. All Rights Reserved.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="payment  text-right">
                            <a href="#"><img src="img/payment/1.png" alt="" /></a>
                            <a href="#"><img src="img/payment/2.png" alt="" /></a>
                            <a href="#"><img src="img/payment/3.png" alt="" /></a>
                            <a href="#"><img src="img/payment/4.png" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright-area start -->
    </footer>
    <!-- FOOTER END -->
</div>
<!-- WRAPPER END -->

<!-- all js here -->
<!-- jquery latest version -->
<script src="{{ asset('js/vendor/jquery-1.12.0.min.js') }}"></script>
<!-- bootstrap js -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- jquery.meanmenu js -->
<script src="{{ asset('js/jquery.meanmenu.js') }}"></script>
<!-- slick.min js -->
<script src="{{ asset('js/slick.min.js') }}"></script>
<!-- jquery.treeview js -->
<script src="{{ asset('js/jquery.treeview.js') }}"></script>
<!-- lightbox.min js -->
<script src="{{ asset('js/lightbox.min.js') }}"></script>
<!-- jquery-ui js -->
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<!-- jquery.nivo.slider js -->
<script src="{{ asset('lib/js/jquery.nivo.slider.js') }}"></script>
<script src="{{ asset('lib/home.js') }}"></script>
<!-- jquery.nicescroll.min js -->
<script src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>
<!-- countdon.min js -->
<script src="{{ asset('js/countdon.min.js') }}"></script>
<!-- wow js -->
<script src="{{ asset('js/wow.min.js') }}"></script>
<!-- plugins js -->
<script src="{{ asset('js/plugins.js') }}"></script>
<!-- main js -->
<script src="{{ asset('js/main.js') }}"></script>
@stack('script')
</body>
</html>
